package db

import (
	"adai.design/jarvis/common/log"
	"github.com/globalsign/mgo"
)

var dbSession *mgo.Session

func GetSession() (*mgo.Session, error) {
	if dbSession != nil {
		return dbSession.Copy(), nil
	}
	var err error
	dbSession, err = mgo.Dial("adai.design:51000")
	if err != nil {
		log.Fatal("%v", err)
	}

	adminDB := dbSession.DB("device")
	adminDB.Login("root", "clouds")

	adminDB = dbSession.DB("home")
	adminDB.Login("root", "clouds")

	adminDB = dbSession.DB("member")
	adminDB.Login("root", "clouds")

	return dbSession.Copy(), nil
}
