package log

import (
	"testing"
	"os"
)

func TestLogger_Output(t *testing.T) {

	std.level = LevelTrace

	t.Logf("gopath: %s", os.Getenv("GOPATH"))
	Printf("let's go!\n")
	Println("let's go!", 123)

	Trace("let's go!")
	Debug("let's go!")
	Info("let's go!")
	InfoR("let's go!")
	InfoW("let's go!")
	Warn("let's go!")
	Error("let's go!")
	defer func() {
		if err := recover(); err == nil {
			t.Fatal("failed")
		} else {
			t.Log(err)
		}
	}()
	Fatal("let's go!")
}