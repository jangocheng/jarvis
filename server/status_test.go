package server

import (
	"runtime"
	"testing"
)

func TestStatusInfo(t *testing.T) {
	var stat runtime.MemStats
	runtime.ReadMemStats(&stat)
	t.Logf("%v", stat)
}
