//
// Create by Zeng Yun on 2018/12/22
// 通信数据包
//

package model

import "fmt"

type Characteristic struct {
	AId   string      `json:"aid"`
	SId   int         `json:"sid"`
	CId   int         `json:"cid"`
	Value interface{} `json:"value"`
}

func ToIntValue(value interface{}) (int, error) {
	switch t := value.(type) {
	case int:
		return t, nil
	case int8:
		return int(t), nil
	case uint8:
		return int(t), nil
	case int32:
		return int(t), nil
	case float32:
		return int(t), nil
	case float64:
		return int(t), nil
	case bool:
		if t == true {
			return 1, nil
		} else {
			return 0, nil
		}
	}
	return 0, fmt.Errorf("ToIntValue: unsupport type value=%v", value)
}

func ToFloatValue(value interface{}) (float32, error) {
	switch t := value.(type) {
	case int:
		return float32(t), nil
	case int8:
		return float32(t), nil
	case uint8:
		return float32(t), nil
	case int32:
		return float32(t), nil
	case float32:
		return t, nil
	case float64:
		return float32(t), nil
	case bool:
		if t == true {
			return 1, nil
		} else {
			return 0, nil
		}
	}
	return 0, fmt.Errorf("ToFloatValue: unsupport threshold type value=%v", value)
}

type ContainerCharacteristic struct {
	Id          string            `json:"container"`
	IsReachable bool              `json:"reachable"`
	Version     int               `json:"version,omitempty"`
	Chars       []*Characteristic `json:"characteristics,omitempty"`
}
