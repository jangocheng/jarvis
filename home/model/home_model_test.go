//
// Create by Zeng Yun on 2018/12/20
//

package model

import (
	"testing"
)

func TestHomeManager(t *testing.T) {
	homes, err := Homes.FindAll()
	if err != nil {
		t.Fatal(err)
	}
	t.Log(homes)

	h, err := Homes.Insert("我的家", "ce173611-1d3d-4414-94ff-1764fc7812f3")
	if err != nil {
		t.Fatal(err)
	}
	t.Log(h)

	hf, err := Homes.FindById(h.Id)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(hf)

	homes, err = Homes.FindAll()
	if err != nil {
		t.Fatal(err)
	}
	t.Log(homes)
}
